<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Title</title>
		<meta charset="UTF-8">
		<meta name=description content="">
		<meta name=viewport content="width=device-width, initial-scale=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Bootstrap CSS -->
		<link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet" media="screen">
        <script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyAkBKdOZaLR5rbl-6aAUmd2n8NgY3YS61E"></script>
        <script>
            function initialize() {
                var mapProp = {
                    center:new google.maps.LatLng(29.1026000,-110.9773200),
                    zoom:10,
                    mapTypeId:google.maps.MapTypeId.ROADMAP
                };
                var map=new google.maps.Map(document.getElementById("googleMap"),mapProp);


                google.maps.event.addListener(map, 'click', function(event) {
                    localizacion(event.latLng);
                });

                function localizacion(posicion) {


                    $("#lat").val(posicion.lat());
                    $("#long").val(posicion.lng());
                }

            }

            google.maps.event.addDomListener(window, 'load', initialize);

        </script>
    </head>

    <body>
    <div class="container">
        <div class="row">
            <div class="col-md-1"></div>
            <div class="col-md-4">
                <form action="/mapa" method="POST" role="form">
                    <legend>Agregar localizacion</legend>
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <div class="form-group">
                        <label for="">Latitud</label>
                        <input type="text" class="form-control" name="latitud" id="lat" placeholder="Latitud">
                    </div>
                    <div class="form-group">
                        <label for="">Longitud</label>
                        <input type="text" class="form-control" name="longitud" id="long" placeholder="Longitud">
                    </div>

                    <button type="submit" class="btn btn-primary">Agregar</button>
                </form>
                <div class="alert alert-info alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <strong>Nota!</strong> Selecciona un punto en el mapa.
                </div>
                <?php $message = Session::get('message')?>
                @if($message == 'exito')
                    <div class="alert alert-success alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <strong>Agregado exitosamente!</strong>
                    </div>
                @endif
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-6">

                <div id="googleMap" style="width:500px;height:380px;"></div>

            </div>
        </div>
    </div>
		<!-- jQuery -->
		<script src="//code.jquery.com/jquery.js"></script>
		<!-- Bootstrap JavaScript -->
		<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
	</body>
</html>