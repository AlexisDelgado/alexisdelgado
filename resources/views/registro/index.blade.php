{{--Formulario de registro para el conductor--}}
{{--extendes hace refencia al a la vista view donde asiganmos @yield lo cual hereda la vista--}}
@extends('welcome')
@section('contenido')
    <form action="{{'registro'}}" method="POST" role="form">
    	<legend>Registrar vehiculo</legend>
            <input type="hidden" name="_token" value="{{csrf_token()}}">
    	<div class="form-group">
    		<label for="">Nombre</label>
    		<input type="text" class="form-control" name="nombre" id="nombre" placeholder="Nombre completo de conductor">
    	</div>
        <div class="form-group">
            <label for="">Direccion</label>
            <input type="text" class="form-control" name="direccion" id="direccion" placeholder="Domicilio ej. urbi #43, Col. Pinos">
        </div>
        <div class="form-group">
            <label for="">Telefono</label>
            <input type="text" class="form-control" name="tel" id="tel" placeholder="Numero de telefono">
        </div>
        <div class="form-group">
            <label for="">Marca</label>
            <input type="text" class="form-control" name="marca" id="marca" placeholder="Marca del auto ej. Ford">
        </div>
        <div class="form-group">
            <label for="">modelo</label>
            <input type="text" class="form-control" name="modelo" id="model" placeholder="Model del auto ej. Lobo">
        </div>
        <div class="form-group">
            <label for="">Año</label>
            <input type="text" class="form-control" name="anio" id="anio" placeholder="Año del auto ej. 2014">
        </div>
        <div class="form-group">
            <label for="">Correo</label>
            <input type="text" class="form-control" name="correo" id="correo" placeholder="Correo ekectronico">
        </div>
        <div class="form-group">
            <label for="">Contraseña</label>
            <input type="password" class="form-control" name="pass" id="pass" placeholder="Contraseña">
        </div>


    	<button type="submit" class="btn btn-primary">Registrar</button>
    </form>
@endsection