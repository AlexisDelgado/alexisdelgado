@extends('welcome')
@section('contenido')
    <?php $mensaje = Session::get('mensaje') ?>
    @if($mensaje == 'exito')
        <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <strong>Usuario registrado exitosamente!</strong>
        </div>
    @endif
    <h1>Bienvenido</h1>
    <div class="form-group">
        <a href="{{'registro'}}" class="btn btn-primary">Registrarme</a>
        <a href="{{'sesion'}}" class="btn btn-success">Iniciar sesion</a>
    </div>
@endsection