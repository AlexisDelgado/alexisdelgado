{{--Vista que muestra el formulario para poder iniciar sesion--}}
@extends('welcome');
@section('contenido')
    <form action="{{'sesion'}}" method="POST" role="form">
    	<legend>Iniciar sesion</legend>
        <input type="hidden" name="_token" value="{{csrf_token()}}">
    	<div class="form-group">
            <label for="">Usuario</label>
            <input type="text" class="form-control" name="usuario" id="usuario" placeholder="Correo electronico">
        </div>
        <div class="form-group">
            <label for="">Contraseña</label>
            <input type="password" class="form-control" name="pass" id="pass" placeholder="Contraseña">
        </div>

    	<button type="submit" class="btn btn-primary">Iniciar sesion</button>
{{--Mensaje de error al iniciar session--}}
        <?php $message = Session::get('message')?>
        @if($message == 'error')
            <div class="alert alert-danger alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <strong>Error al iniciar sesion!</strong> Intentelo de nuevo
            </div>
        @endif

    </form>
@endsection