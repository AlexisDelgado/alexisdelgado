<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Location extends Model {

    protected $fillable = ['latitud','longitud']; // Columnas a llenar en la peticiones por post

    public function user(){
        return $this->belongsTo('User'); //Esta funcion hace refeencia ala table User  que cada localizacion pertenece a un usuario
    }
}
