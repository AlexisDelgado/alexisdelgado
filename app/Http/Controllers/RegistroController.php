<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;

class RegistroController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view('registro.index');//Regresa a la vista principal de registro
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
        //Almacenamos los datos del conductor en ele registro
		$usuario = new User();

        $usuario->nombre = $request->nombre;
        $usuario->direccion =$request->direccion;
        $usuario->tel = $request->tel;
        $usuario->marca = $request->marca;
        $usuario->modelo = $request->modelo;
        $usuario->anio = $request->anio;
        $usuario->correo = $request->correo;
        $usuario->password = Crypt::encrypt($request->pass); //Encriptamos la contrasecon con la clase Crypt
        if( $usuario->save() == true ){
            return redirect('/')->with('mensaje','exito');
        }
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
