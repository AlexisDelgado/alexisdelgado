<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Route::get('/', 'WelcomeController@index');
//
// Route::get('home', 'HomeController@index');
//
// Route::controllers([
// 	'auth' => 'Auth\AuthController',
// 	'password' => 'Auth\PasswordController',
// ]);

Route::get('/','WelcomeController@index'); //Controlador para modelo y vista welcome pagina princial
Route::resource('/registro','RegistroController'); //Controlador para la vista registro y almacenamiento de los datos del conductor
Route::resource('/sesion','SesionController');//Controlador para el inicio de sesion del conducto
Route::resource('/mapa','MapaController');//Controlador para la vista mapa y alamecenamiento de localizaciones
//Route::resource('/mapa','MapaController@store');

